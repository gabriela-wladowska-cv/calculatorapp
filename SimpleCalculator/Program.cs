﻿namespace SimpleCalculator
{
    using System;
    using System.Globalization;
    using Model;

    public class Program
    {
        public static void Main()
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US");
            CultureInfo.CurrentUICulture = new CultureInfo("en-US");

            Communication.WelcomeUser();
            Communication.ShowMenu();

            var pressedKey = Console.ReadKey().Key;

            try
            {
                var chosenOperation = Communication.GetOperationFromUser(pressedKey);

                Console.WriteLine($"\nChosen operation: {chosenOperation}");

                switch (chosenOperation)
                {
                    case Operation.Addition:
                    {
                        Console.Write("Enter first addend: ");

                        var firstAddend = Communication.GetNumberFromUser();

                        Console.Write("Enter second addend: ");

                        var secondAddend = Communication.GetNumberFromUser();
                        var sum = Calculation.AddNumbers(firstAddend, secondAddend);

                        Console.WriteLine($"Sum: {sum}");
                        break;
                    }
                    case Operation.Subtraction:
                    {
                        Console.Write("Enter minuend: ");

                        var minuend = Communication.GetNumberFromUser();

                        Console.Write("Enter subtrahend: ");

                        var subtrahend = Communication.GetNumberFromUser();
                        var difference = Calculation.SubtractNumbers(minuend, subtrahend);

                        Console.WriteLine($"Difference: {difference}");
                        break;
                    }
                    case Operation.Multiplication:
                    {
                        Console.Write("Enter first factor: ");

                        var firstFactor = Communication.GetNumberFromUser();

                        Console.Write("Enter second factor: ");

                        var secondFactor = Communication.GetNumberFromUser();
                        var product = Calculation.MultiplyNumbers(firstFactor, secondFactor);

                        Console.WriteLine($"Product: {product}");
                        break;
                    }
                    case Operation.Division:
                    {
                        Console.Write("Enter dividend: ");

                        var dividend = Communication.GetNumberFromUser();

                        Console.Write("Enter divisor: ");
                        var divisor = Communication.GetNumberFromUser();
                        var quotient = Calculation.DivideNumbers(dividend, divisor);

                        Console.WriteLine($"Quotient: {quotient}");
                        break;
                    }
                    default:
                    {
                        throw new Exception("This action is not currently supported.");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"\nAn error occured in application. Error message:\n{e.Message}");
                Environment.Exit(1);
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
