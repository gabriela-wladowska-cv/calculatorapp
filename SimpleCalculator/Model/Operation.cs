﻿namespace SimpleCalculator.Model
{
    public enum Operation
    {
        Addition,
        Subtraction,
        Multiplication,
        Division
    }
}
