﻿namespace SimpleCalculator
{
    using System;
    using Model;

    public static class Communication
    {
        public static void WelcomeUser()
        {
            Console.WriteLine("Welcome!");
        }

        public static void ShowMenu()
        {
            Console.WriteLine("Choose an operation by pressing related key:");
            Console.WriteLine($"\"a\" - {Operation.Addition}");
            Console.WriteLine($"\"s\" - {Operation.Subtraction}");
            Console.WriteLine($"\"m\" - {Operation.Multiplication}");
            Console.WriteLine($"\"d\" - {Operation.Division}");
        }

        public static Operation GetOperationFromUser(ConsoleKey pressedKey)
        {
            switch (pressedKey)
            {
                case ConsoleKey.A:
                {
                    return Operation.Addition;
                }
                case ConsoleKey.S:
                {
                    return Operation.Subtraction;
                }
                case ConsoleKey.M:
                {
                    return Operation.Multiplication;
                }
                case ConsoleKey.D:
                {
                    return Operation.Division;
                }
                default:
                {
                    throw new Exception("Operation does not exist.");
                }
            }
        }

        public static decimal GetNumberFromUser()
        {
            var enteredValue = Console.ReadLine();

            if (decimal.TryParse(enteredValue, out var number))
            {
                return number;
            }

            throw new Exception("Entered value is not correct.");
        }
    }
}
