﻿namespace SimpleCalculator
{
    using System;

    public static class Calculation
    {
        public static decimal AddNumbers(decimal firstAddend, decimal secondAddend)
        {
            return firstAddend + secondAddend;
        }

        public static decimal SubtractNumbers(decimal minuend, decimal subtrahend)
        {
            return minuend - subtrahend;
        }

        public static decimal MultiplyNumbers(decimal firstFactor, decimal secondFactor)
        {
            return firstFactor * secondFactor;
        }

        public static decimal DivideNumbers(decimal dividend, decimal divisor)
        {
            if (divisor != 0)
            {
                return dividend / divisor;
            }

            throw new Exception("Divisor cannot be equal zero.");
        }
    }
}
